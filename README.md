# termite-wrapper

Termite wrapper that allows to execute a command line in termite. Supports commands containing whitespace. Useful when using termite for running desktop files.